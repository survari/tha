//
// Created by survari on 08.02.19.
//

#ifndef THA_THA_HPP // THA = Triploit Hash Algorithm
#define THA_THA_HPP

#include <string>
#include <sstream>
#include <vector>
#include <cmath>


namespace tri
{
    static struct THA
    {
        static inline int MAX_LEN_THA = 64;
    } THA;

    namespace tha
    {
        std::string hex_to_string(int i)
        {
            std::stringstream sst;
            sst << std::hex << i;

            std::string res = sst.str();

            return (res.size() == 1 ? "0"+res : res);
        }

        std::string print_finished_vector(std::vector<int> v)
        {
            std::string res;

            for (int i : v)
                res += hex_to_string(i);

            return res;
        }

        bool is_ascii_okay(const std::vector<int> &v)
        {
            for (int i : v)
            {
                if (i > UINT8_MAX || i < 0)
                    return false;
            }

            return true;
        }

        std::vector<int> adjust_char(int c, int depth=0)
        {

            std::vector<int> res;

            if (c > UINT8_MAX)
            {
                int a1 = c / 2 + (int) sqrt(c);
                int a2 = c / 2 - (int) sqrt(c);

                if (!is_ascii_okay({a1}))
                    a1 = adjust_char(a1, depth+1)[0];

                if (!is_ascii_okay({a2}))
                    a2 = adjust_char(a2, depth+1)[0];

                res.push_back(a1);
                res.push_back(a2);
            }
            else if (c < 0)
            {
                c = c * (-1);
                res.push_back(c);
            }
            else
                res.push_back(c);

            return res;
        }

        std::vector<int> adjust_array(std::vector<int> v, int m)
        {

            if (v.size() > THA.MAX_LEN_THA)
            {
                std::vector<int> too_much;

                for (int c = 0; c < v.size(); c++)
                {
                    if ((c+1) > THA.MAX_LEN_THA)
                    {
                        too_much.push_back(v[c]);
                        v.erase(v.begin()+c);
                    }
                }


                if (too_much.size() != THA.MAX_LEN_THA &&
                    too_much.size() != 0)
                    too_much = adjust_array(too_much, too_much.size());

                for (int c = 0; c < v.size(); c++)
                {
                    int b = v[c];

                    if (too_much.size() >= 1)
                    {
                        v[c] = adjust_char(v[c] + too_much[c])[0];
                    }
                    else
                        return {};
                }

            }
            else if (v.size() < THA.MAX_LEN_THA)
            {
                for (int c = 0; c < THA.MAX_LEN_THA; c++)
                {
                    if (c >= v.size())
                    {
                        int n = 0;
                        int e = 0;

                        if (v.size() == 0)
                            n = 1;
                        else
                            n = v[c-1];

                        if (n == 0)
                            n = 2;

                        int n2 = 0;

                        if (v.size() <= 1)
                            n2 = 2;
                        else
                            n = v[c-2];

                        if (n2 == 0)
                            n2 = 1;

                        int magic = (int) std::abs(sin(m * n2 / (v.size()+1) + c) * 100);

                        if ((magic % 2) == 0)
                        {
                            if (((n*n2) % 2) == 0)
                                e = (int) (std::abs(sin(0.08 * (n * n2 + c * v.size()))) * 255);
                            else
                                e = (int) (std::abs(sin(0.08 * (n / n2 - c + v.size()))) * 255);

                            if (e == 0)
                                e = n2 * 3 * c - 1 / n;
                        }
                        else if ((magic % 3) == 0)
                        {
                            if ((n % 2) == 0)
                                e = (int) ((n * c) + sqrt(n / (c + 1) + n2) - sqrt(v.size() * (2*M_PI)));
                            else
                                e = (int) ((n2 + c * n) / ((n * sqrt(c * n2))+1) - sqrt(v.size() * (2*M_PI)));

                            if (e == 0)
                                e = n2 * c + n2;
                        }
                        else if ((magic % 5) == 0)
                        {
                            if ((n % 2) == 0)
                                e = (int) ((n2 + c * n) / ((n * sqrt(c * n2))+1) - sqrt(v.size() * M_PI));
                            else
                                e = (int) ((n * c) + sqrt(n / (c + 1) + n2) - sqrt(v.size() * M_PI));

                            if (e == 0)
                                e = n2 / c + c * n2;
                        }
                        else
                        {
                            if ((n % 2) == 0)
                                e = (int) (std::abs(sin(0.08 * (n * c + n2 - 4 * M_PI * n)) * 250));
                            else
                                e = (int) (std::abs(sin(0.08 * (n * c - n2 + 3 * M_PI * (n/3))) * 250));

                            if (e == 0)
                                e = 2 * n2 * c + c;
                        }

                        v.push_back(adjust_char(e)[0]);
                    }
                }
            }

            return v;
        }

        std::vector<int> adjust_chars(const std::vector<int> &v)
        {

            std::vector<int> copy;

            for (int c : v)
            {
                std::vector<int> a = adjust_char(c);
                copy.insert(copy.begin(), a.begin(), a.end());
            }

            return copy;
        }

        std::vector<int> hash_int_vector(std::vector<int> chars)
        {
            chars = adjust_array(chars, chars.size()+chars[0]);
            chars = adjust_chars(chars);

            while (chars.size() > THA.MAX_LEN_THA || !is_ascii_okay(chars))
            {
                chars = adjust_array(chars, chars.size()+(chars[0]*2));

                if (!is_ascii_okay(chars))
                    chars = adjust_chars(chars);
            }

            std::vector<int> step1;

            for (int i = 0; i < chars.size(); i++)
            {
                double n = M_PI;
                double l = 2*M_PI;
                double t = chars[i];
                double s = chars.size();

                if ((i+1) < chars.size())
                    n = chars[i+1];

                if ((i-1) >= 0)
                    n = chars[i-1];

                int o = 0;

                if ((i % 2) == 0)
                    o = (int) (chars.size() * n - l * (t / n + l) * M_PI * M_E / (n+l-t));
                else if ((i % 3) == 0)
                    o = (int) (chars.size() * n + l * (t / n - l) * M_PI * M_E / (n-l+t));
                else
                    o = (int) (((chars.size() * 2) * n * l / (t / n + l) - (M_PI * M_E) + (n*l-s*t)) / 2);

                if (o < 0)
                    o = o * (-1);

                step1.insert(step1.begin(), adjust_char(o)[0]);
            }

            std::vector<int> step2 = step1;

            int h = step1[0];

            if (h == 0)
                h = 3;

            for (int c = 0; c < h; c++)
            {
                for (int i = 0; i < step2.size(); i++)
                {
                    if ((i+3) < step2.size())
                    {
                        step2.insert(step2.begin()+i+3, step2[i]);
                        step2.erase(step2.begin()+i);
                    }
                }

                std::vector<int> s2_1(step2.begin(), step2.end()-(step2.size()/2));
                std::vector<int> s2_2(step2.end()-(step2.size()/2), step2.end());

                s2_1 = adjust_array(s2_1, s2_1[0]);
                s2_2 = adjust_array(s2_2, s2_2[0]);

                for (int i = 0; i < s2_1.size(); i++)
                {
                    step2[i] = adjust_char(s2_1[i] + s2_2[i])[0];
                }

                for (int i = 0; i < step2.size(); i++)
                {
                    if ((i+3) < step2.size())
                    {
                        step2.insert(step2.begin(), step2[i]);
                        step2.erase(step2.begin()+i+1);
                    }
                }
            }

            chars = step2;
            return chars;
        }

        std::string hash(const std::wstring &str)
        {
            std::vector<int> chars(str.begin(), str.end());

            if (str.empty() || chars.empty())
                chars.push_back(0);

            chars = hash_int_vector(chars);

            std::string res;

            for (int i : chars)
                res += hex_to_string(i);

            return res;
        }

        std::string hash(const std::string &str)
        {
            std::vector<int> chars(str.begin(), str.end());

            if (str.empty() || chars.empty())
                chars.push_back(0);

            chars = hash_int_vector(chars);

            std::string res;

            for (int i : chars)
                res += hex_to_string(i);

            return res;
        }

        std::vector<int> add_array(std::vector<int> v, std::vector<int> tmp)
        {
            unsigned long max = 0;

            // std::cout << "v   = " << v.size() << std::endl;
            // std::cout << "tmp = " << tmp.size() << std::endl;

            if (v.size() == tmp.size())
            {
                max = v.size();
            }
            else
                max = (v.size() > tmp.size() ? v.size() : tmp.size());

            std::vector<int> ret;

            for (unsigned long i = 0; i < max; i++)
            {
                if (i >= tmp.size() && i < v.size())
                {
                    ret.push_back(v[i]);
                }
                else if (i < tmp.size() && i >= v.size())
                {
                    ret.push_back(tmp[i]);
                }
                else if (i < tmp.size() && i < v.size())
                {
                    ret.push_back(adjust_char(v[i] + tmp[i])[0]);
                    ret = adjust_array(ret, ret.size());
                }
                else
                {
                    ret.push_back(0);
                }
            }

            return adjust_array(ret, ret.size());
        }

        std::string print_vector(std::vector<int> v)
        {
            std::string ret = "";

            for (int i : v)
                ret += std::to_string(i)+"\n";

            return ret;
        }
    }
}

#endif //THA_THA_HPP
