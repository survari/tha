#include <iostream>
#include <fstream>
#include <cstring>
#include <codecvt>
#include "tha.hpp"

#define VERSION "0.0.1a"

void help_page()
{
    std::cout <<
    "THA - Triploit Hash Algorithm - Version " VERSION "\n\n"
    "Usage:\n\n"
    "    -h, --help         - Shows this help page.\n"
    "    -v, --version      - Shows the version of the program.\n\n"
    "    -l, --length <int> - Sets the length of the hashes.\n"
    "                         Will get slower, if it gets bigger.\n"
    "                         Standard is 64.\n\n"
    "    -b, --big          - Calculate long-block for files.\n"
    "    -f, --file <file>  - Generates the hash value of a file.\n"
    << std::endl;
}

int main(int argc, char* argv[])
{
    bool _do_long = false;

    if (argc <= 1)
    {
        std::cout << "error: to few arguments." << std::endl;
        help_page();
    }

    for (int i = 1; i < argc; i++)
    {
        if (argv[i][0] == '\\' && argv[i][1] == '-')
        {
            std::string tmp;

            while (i < argc && argv[i][0] != '-')
            {
                tmp += std::string(argv[i]) + " ";
                i++;
            }

            tmp = tmp.substr(1, tmp.size()-1);

            std::cout << "==== FOR STRING: \"" << tmp << "\"" << std::endl << std::endl;
            std::cout << tri::tha::hash(tmp) << std::endl << std::endl;
        }
        else if (argv[i][0] == '-')
        {
            if (strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--file") == 0)
            {
                if ((i+1) >= argc)
                {
                    std::cout << "error: to few arguments. no file was given." << std::endl;
                    help_page();
                    return 1;
                }

                std::ifstream infile(argv[i+1], std::ios::binary);
                std::vector<int> binary((std::istreambuf_iterator<char>(infile)), (std::istreambuf_iterator<char>()));

                if (!infile.is_open())
                {
                    std::cout << "error: file not found or not readable!" << std::endl;
                    return 1;
                }

                std::cout << "==== FOR FILE: \"" << argv[i+1] << "\"" << std::endl;

                std::vector<std::vector<int>> _long;

                std::vector<int> content;
                std::vector<int> tmp;

                std::string block;
                int line = 1;
                int count = 0;

                int cc = 0;

                for (int c : binary)
                {
                    if ((count % 10) == 0)
                    {
                        std::string s;

                        switch(cc)
                        {
                            case 0:
                                s = "-";
                                break;
                            case 1:
                                s = "-";
                                break;
                            case 2:
                                s = "\\";
                                break;
                            case 3:
                                s = "\\";
                                break;
                            case 4:
                                s = "|";
                                break;
                            case 5:
                                s = "|";
                                break;
                            case 6:
                                s = "/";
                                break;
                            case 7:
                                s = "/";
                                break;
                            default:
                                s = "- ";
                                cc = 0;
                                break;
                        }

                        std::cout << "calculating " << s << std::endl;
                        std::cout << "\033[1A";

                        cc++;
                    }

                    count++;

                    if (c == 10)
                        line++;

                    if (count == tri::THA.MAX_LEN_THA)
                    {
                        content.push_back(c);

                        // std::cout << "=============== [[ [[" << line << "]] " << std::endl << block << " ]]" << std::endl << std::endl;
                        tmp = tri::tha::add_array(content, tmp);
                        _long.push_back(tmp);

                        content.clear();
                        block = "";
                        count = 0;
                    }
                    else
                        content.push_back(c);

                    block += (char) c;
                }

                if (count != 0)
                {
                    // std::cout << "=============== [[ [[" << line << "]] " << std::endl << block << " ]]" << std::endl << std::endl;
                    tmp = (tri::tha::add_array(content, tmp));
                    _long.push_back(tmp);
                    content.clear();
                }

                std::vector<int> e = tri::tha::hash_int_vector(tmp);
                _long.push_back(e);

                if (_do_long)
                {
                    std::cout << "Long:           " << std::endl << std::endl;

                    for (int x = 0; x < _long.size(); x++)
                    {
                        if (_long.size() < 8)
                        {
                            if (_long.size() == 1)
                            {
                                std::cout << tri::tha::print_finished_vector(tri::tha::hash_int_vector(_long[x])) << std::endl;
                                break;
                            }
                            else
                            {
                                std::cout << tri::tha::print_finished_vector(tri::tha::hash_int_vector(_long[x])) << std::endl;
                            }
                        }
                        else
                        {
                            if ((x % (int) round(_long.size()/8)) == 0)
                            {
                                std::cout << tri::tha::print_finished_vector(tri::tha::hash_int_vector(_long[x])) << std::endl;
                            }
                        }
                    }
                }

                // std::cout << tri::tha::print_vector(tmp) << std::endl;

                if (!_do_long)
                    std::cout << "\033[1A";

                std::cout << std::endl << "Short:            " << std::endl << std::endl;
                std::cout << tri::tha::print_finished_vector(e) << std::endl << std::endl;

                i++;
            }
            else if (strcmp(argv[i], "-b") == 0 || strcmp(argv[i], "--big") == 0)
            {
                _do_long = true;
            }
            else if (strcmp(argv[i], "-l") == 0 || strcmp(argv[i], "--length") == 0)
            {
                if ((i+1) >= argc)
                {
                    std::cout << "error: to few arguments. no file was given." << std::endl;
                    help_page();
                    return 1;
                }

                tri::THA.MAX_LEN_THA = std::stoi(argv[i+1]);
                i++;
            }
            else if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--version") == 0)
            {
                std::cout << VERSION << std::endl;
            }
            else if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
            {
                help_page();
            }
            else
            {
                std::cout << "error: argument \"" << argv[i] << "\" not found." << std::endl;
                help_page();
                return 1;
            }
        }
        else
        {
            std::string tmp;

            while (i < argc && argv[i][0] != '-')
            {
                tmp += std::string(argv[i]) + " ";
                i++;
            }

            tmp = tmp.substr(0, tmp.size()-1);

            std::cout << "==== FOR STRING: \"" << tmp << "\"" << std::endl << std::endl;
            std::cout << tri::tha::hash(tmp) << std::endl << std::endl;

            if (i >= argc)
                return 0;

            if (argv[i][0] == '-')
            {
                i--;
                continue;
            }
        }
    }

    return 0;
}